package com.example.brst_pc17.zumespot.Async_Thread;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.example.brst_pc17.zumespot.MyUtilities.MyUtil;
import com.example.brst_pc17.zumespot.R;
import org.json.JSONException;
import org.json.JSONObject;

/*import com.appalmighty.ezefind.R;
import com.appalmighty.ezefind.ezefind.util.Bean;
import com.appalmighty.ezefind.ezefind.util.MyUtil;*/
/*import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;*/


public class Upload_Files_AsyncTask extends AsyncTask<Void, Void, String>
{

    Context con;

//    Bean bean;
    ProgressDialog dialog;
    Super_AsyncTask_Interface listener = null;
    String Upload_Url;
    int attachmentCount=0;

  /*  public Upload_Files_AsyncTask(Context context, Bean bean, String Upload_Url, Super_AsyncTask_Interface listener)
    {
        this.con = context;
        this.bean = bean;
        this.listener = listener;
        this.Upload_Url = Upload_Url;

    }*/

    protected void onPreExecute()
    {

        dialog = ProgressDialog.show(con, "", "");
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.progress_dialog);
        dialog.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params)
    {
        String response = "";
        try
        {

//            response = executeMultipartPost(Upload_Url);

        }
        catch (Exception e)
        {
            Log.e("Exception issss", "" + e.toString());

        }

        return response;
    }



    @Override
    protected void onPostExecute(String ResponseString)
    {
        super.onPostExecute(ResponseString);

        Log.e("Response+++++++++",ResponseString);

        if (dialog.isShowing())
        {
            dialog.dismiss();
        }

        if (ResponseString.contains("true"))
        {
            listener.onTaskCompleted(ResponseString);

        }
        else if (ResponseString.equals("SLOW"))
        {
            MyUtil.show_Toast(con,"Please check your network.");
        }
        else if (ResponseString.equalsIgnoreCase("ERROR"))
        {
            MyUtil.show_Toast(con,"Server side error.");
        }
        else
        {
            try
            {
                JSONObject object = new JSONObject(ResponseString);

                MyUtil.show_Toast(con,object.getString("Message").toString());
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

    }

    /*public String executeMultipartPost(String url) throws Exception
    {
        try
        {

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url);

            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            HashMap<String, String> map = bean.getMap_addDataItems();

            for (String key : map.keySet())
            {
                reqEntity.addPart(key, new StringBody(map.get(key)));
            }


            if(bean.getImage() != null)
            {

                ByteArrayBody mImageByteArray = new ByteArrayBody(bean.getImage(), Long.toString(System.currentTimeMillis()) + ".jpg");

                reqEntity.addPart("Image", mImageByteArray);
            }



            for (int i = 0; i < bean.getAttachmentImagesList().size() ; i++)
            {
                ByteArrayBody mImageByteArray1 = new ByteArrayBody(bean.getAttachmentImagesList().get(i), Long.toString(System.currentTimeMillis()) + ".jpg");

                reqEntity.addPart("Attachment"+attachmentCount,mImageByteArray1);
                attachmentCount++;
            }

            for (int i = 0; i < bean.getAudioPath_list().size() ; i++)
            {
                FileBody fileBody = new FileBody(new File(bean.getAudioPath_list().get(i)));

                reqEntity.addPart("Attachment"+attachmentCount,fileBody);
                attachmentCount++;
            }


            String barcodeString="";
            for (int i = 0; i < bean.getBarcode_list().size() ; i++)
            {
                barcodeString=barcodeString.isEmpty()? bean.getBarcode_list().get(i) : barcodeString+","+bean.getBarcode_list().get(i);

//                reqEntity.addPart("Barcode"+attachmentCount,new StringBody(bean.getBarcode_list().get(i)));

//                attachmentCount++;
            }

            if(!barcodeString.isEmpty())
            {
                Log.e("Barcode",""+barcodeString);
                reqEntity.addPart("Barcode",new StringBody(barcodeString));
            }


            postRequest.setEntity(reqEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();

            while ((sResponse = reader.readLine()) != null)
            {
                s = s.append(sResponse);
            }

            Log.e("*************", "Response: " + s);
            return s.toString();
        }
        catch (Exception e)
        {
            Log.e("Error",""+e.getStackTrace());

            return "ERROR";
        }
    }
*/
}