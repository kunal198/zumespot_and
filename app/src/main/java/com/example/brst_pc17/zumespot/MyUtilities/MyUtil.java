package com.example.brst_pc17.zumespot.MyUtilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask;
import com.example.brst_pc17.zumespot.Async_Thread.Upload_Files_AsyncTask;
import com.example.brst_pc17.zumespot.R;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by brst-pc17 on 8/27/16.
 */
public class MyUtil {

    public static Toast toast;

    //    To get key hash************************************************************************************************
    public void getKeyHash(Context con) {
        try {

            PackageInfo info = con.getPackageManager().getPackageInfo(con.getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {
            Log.e("name not found", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        }
    }


    // To show Toast ****************************************************************************************************
    public static void show_Toast(Context con, String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(con, text, Toast.LENGTH_SHORT);

        toast.show();
    }


    public static void execute(Super_AsyncTask asyncTask) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            asyncTask.execute();
        }

    }

    public static void execute2(Upload_Files_AsyncTask asyncTask) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            asyncTask.execute();
        }
    }


    // To hide Keyboard *************************************************************************************************
    public void hide_keyboard(Context context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            if (inputMethodManager.isAcceptingText()) {
                inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void setImage(Context context, ImageView imageView, String url) {

        /*if (url == null && url.isEmpty()) {
            Picasso.with(context).load("null").placeholder(R.drawable.loading) // optional
                    .error(R.drawable.no_image).into(imageView);
        } else {*/
        Picasso.with(context).load(url).placeholder(R.drawable.loading) // optional
                .error(R.drawable.no_image)
                .into(imageView);

//        }

    }


    public String changeInstaDate(String unixSeconds) {
        Date date = new Date(Long.parseLong(unixSeconds) * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);

        Log.e("Date", "" + formattedDate);

        return formattedDate;

    }


    public static void get_location(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        Log.e("Current", "LAT" + latitude);
        Log.e("Current", "LONG" + longitude);


    }


    public void setImageUrl(Context context, ImageView imageView, String url) {

        /*if (url == null && url.isEmpty()) {
            Picasso.with(context).load("null").placeholder(R.drawable.loading) // optional
                    .error(R.drawable.no_image).into(imageView);
        } else {*/
       /* Picasso.with(context).load(url).placeholder(R.drawable.loading) // optional
                .error(R.drawable.no_image)
                .into(imageView);*/

        Glide.with(context).load(url).placeholder(R.drawable.loading).error(R.drawable.no_image).into(imageView);

//        }

    }

}












    /*@Override
    public View getView(final int position, View row, ViewGroup parent) {
        LayoutInflater inflater = null;
        Viewholder viewholder = null;
        // if(row==null) {

        if (row == null) {
            viewholder = new Viewholder();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.custom_activity_main, parent, false);
            viewholder.imgv_users = (ImageView) row.findViewById(R.id.imgv_users);
            viewholder.title = (TextView) row.findViewById(R.id.title);
            viewholder.distance = (TextView) row.findViewById(R.id.distance);

            String type = list.get(position).get(MyConstant.TYPE);
            Log.e("received","imgurl"+list.get(position).get(MyConstant.InstagramPicUrl));

            if (list.get(position).get(MyConstant.TYPE).equals("InstagramUser")) {
                String imgUrl = list.get(position).get(MyConstant.FacewbookPicUrl);
                myUtil.setImage(context, viewholder.imgv_users, imgUrl);


                viewholder.title.setText(list.get(position).get(MyConstant.InstagramFullName) + " " + MyConstant.INSTAGRAM_USER);
                mul = Integer.parseInt(list.get(position).get(MyConstant.InstagramDistance));

                newVal = String.valueOf(mul * 0.00056818);


                viewholder.distance.setText(String.format("%.6f", Double.parseDouble(newVal)) + " Miles");

                viewholder.title.setSelected(true);

            } else if (list.get(position).get(MyConstant.TYPE).equals("FacebookUser")) {

                String imgUrl = list.get(position).get(MyConstant.FacewbookPicUrl);

                Log.e("adapter", "imgurl" + imgUrl + " " + type + "pos " + position);
                myUtil.setImage(context, viewholder.imgv_users, imgUrl);


                if (list.get(position).get(MyConstant.FacebookUserName).contains("@")) {
                    String[] separated = list.get(position).get(MyConstant.FacebookUserName).split("@");
                    separated[1] = separated[1].trim();
                    viewholder.title.setText(separated[1] + " " + MyConstant.FACEBOOK_USER);
                } else {
                    viewholder.title.setText(list.get(position).get(MyConstant.FacebookUserName) + " " + MyConstant.FACEBOOK_USER);

                }
                newVal = String.valueOf(mul * 0.00056818);
                viewholder.distance.setText(String.format("%.6f", Double.parseDouble(newVal)) + " Miles");
                viewholder.title.setSelected(true);
            } else if (list.get(position).get(MyConstant.TYPE).equals("TwitterUser")) {
                String imgUrl = list.get(position).get(MyConstant.TwitterPicUrl);


                myUtil.setImage(context, viewholder.imgv_users, imgUrl);
                if (list.get(position).get(MyConstant.TwitterUserName).contains("@")) {
                    String[] separated = list.get(position).get(MyConstant.TwitterUserName).split("@");
                    separated[1] = separated[1].trim();
                    viewholder.title.setText(separated[1] + " " + MyConstant.TWITTER_USER);
                } else {
                    viewholder.title.setText(list.get(position).get(MyConstant.TwitterUserName) + " " + MyConstant.TWITTER_USER);

                }
                newVal = String.valueOf(mul * 0.00056818);
                viewholder.distance.setText(String.format("%.6f", Double.parseDouble(newVal)) + " Miles");

            }

            row.setTag(viewholder);

        } else {
            viewholder = (Viewholder) row.getTag();
        }*/


        //