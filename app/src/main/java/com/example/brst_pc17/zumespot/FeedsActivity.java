package com.example.brst_pc17.zumespot;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc17.zumespot.Adapters.FeedsAdapter;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask_Interface;
import com.example.brst_pc17.zumespot.MyUtilities.MyConstant;
import com.example.brst_pc17.zumespot.MyUtilities.MyUtil;
import com.example.brst_pc17.zumespot.MyUtilities.StaggeredGridView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FeedsActivity extends AppCompatActivity implements View.OnClickListener {

    Context context;


    TextView txtv_header_title;
    String facebookUsernme, fbUsernme, instagramId, twiterUsername;

    StaggeredGridView staggeredGridView;

    ArrayList<HashMap<String, String>> feedsList;
    private GoogleApiClient client;
    String media_url;
    ImageView load_more;

    int loadsize=10;
    int first=0;
    int loadmethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds);

        context = this;

        setUpIDS();
        instagramId = getIntent().getStringExtra(MyConstant.InstagramId);
        facebookUsernme = getIntent().getStringExtra(MyConstant.FacebookUserName);
        twiterUsername = getIntent().getStringExtra(MyConstant.TwitterUserName);

        Log.e("FacebookUserNameRes", "facebookUsernme" + facebookUsernme);
        Log.e("instagramIdRes", "instagramId" + instagramId);


        if (instagramId != null) {
            getInstagramFeedsUsingUserId(instagramId);

        } else if (facebookUsernme != null) {
            String[] separated = facebookUsernme.split("@");
            fbUsernme = separated[1].trim();

            Log.e("final", "facebook" + fbUsernme);
            facebookFeeds(fbUsernme);
        } else if (twiterUsername != null) {
            getTwitterData(twiterUsername);
        }


        txtv_header_title.setText(getIntent().getStringExtra(MyConstant.InstagramFullName));
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    public void setUpIDS() {

        findViewById(R.id.imgv_toggle).setOnClickListener(this);

        txtv_header_title = (TextView) findViewById(R.id.txtv_header_title);
        txtv_header_title.setSelected(true);

        staggeredGridView = (StaggeredGridView) findViewById(R.id.gridView_search);
        staggeredGridView.setOnScrollListener(scrollListener);

        load_more=(ImageView) findViewById(R.id.load_more);
        load_more.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgv_toggle:

                finish();

                break;

            case R.id.load_more:


                if(loadmethod==1){
                    getInstagramFeedsUsingUserId(instagramId);
                    first = loadsize + 1;
                    loadsize = 19;
                }

            else    if(loadmethod==2){
                    facebookFeeds(fbUsernme);
                    first = loadsize + 1;
                    loadsize = 19;
                }
             else   if (loadmethod==3) {
                    getTwitterData(twiterUsername);
                    first = loadsize + 1;
                    loadsize = 19;
                }

               /* if (loadsize==19){

                    Toast.makeText(context,"No more feeds .",Toast.LENGTH_SHORT).show();
                }*/

                load_more.setVisibility(View.GONE);
                break;
        }
    }


    public void setData() {

        Log.e("inset", "dataAdapter"+feedsList.size());
        FeedsAdapter item;
        for (int i = 0; i < feedsList.size(); i++) {
            item = new FeedsAdapter(context, feedsList.get(i));//,likesID.get(i));
            staggeredGridView.addItem(item);
        }
    }


    private StaggeredGridView.OnScrollListener scrollListener = new StaggeredGridView.OnScrollListener() {
        public void onTop() {
        }

        public void onScroll() {

        }

        public void onBottom() {
            //            loadMoreData();
        }
    };


    //*****************************************************  service  *************************************************************************


    public void getInstagramFeedsUsingUserId(String userId) {

        Log.e("inInstafeeds", "service");

        String url = MyConstant.getInstagramFeedsUrl(userId);
        // String url = MyConstant.getFacebookProfile();
        Log.e("Index : ", url);


        loadmethod=1;
        MyUtil.execute(new Super_AsyncTask(context, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {
                try {
                    JSONObject object = new JSONObject(output);
                    Log.e("insta", "output" + output);
                    if (object.getJSONObject("meta").getString("code").equals("200")) {

                        feedsList = new ArrayList<>();
                        JSONArray array = object.getJSONArray("data");






                        for (int i = first; i <= loadsize; i++) {
                            JSONObject obj = array.getJSONObject(i);

                            Log.e("obj","PrintObj"+obj);



                            String id=obj.optString("id");

                            Log.e("mediaID","ID"+id);

                            String user_has_liked=obj.optString("user_has_liked");
                            HashMap<String, String> map = new HashMap<>();

                            map.put(MyConstant.FeedPicUrl, obj.getJSONObject("images").getJSONObject("low_resolution").getString(MyConstant.FeedPicUrl));
                            map.put(MyConstant.FeedCaptionText, obj.getJSONObject("caption").getString(MyConstant.FeedCaptionText));
                            map.put(MyConstant.FeedTime, obj.getJSONObject("caption").getString(MyConstant.FeedTime));
                            map.put(MyConstant.FeedCommentCount, obj.getJSONObject("comments").getString("count"));
                            map.put(MyConstant.FeedLikeCount, obj.getJSONObject("likes").getString("count"));
                            map.put(MyConstant.FeedMediaId,id);
                            map.put(MyConstant.UserHasLiked,user_has_liked);

                            map.put(MyConstant.TYPE, MyConstant.INSTAGRAM_USER);

                            feedsList.add(map);
                        }
                        Log.e("instagramFeed", "list" + feedsList.toString());

                        setData();

                    } else {

                    }
                } catch (Exception ex) {

       Log.e("Exception is", ex.toString());
                }
            }
        }, true));
    }


    public void facebookFeeds(String userId) {
        Log.e("inFacebookfeeds", "service");
        String url = MyConstant.getFacebookFeeds(userId);
        Log.e("urlCreated : ", url);


        MyUtil.execute(new Super_AsyncTask(context, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                Log.e("facebook", "outputres" + output);
                loadmethod=2;
                String commentCount = null;

                try {
                    JSONObject object = new JSONObject(output);

                    feedsList = new ArrayList<>();

                    JSONArray array = object.optJSONArray("data");
                    Log.e("facebook", "array" + array);


                    for (int i = first; i <= loadsize; i++) {
                        JSONObject obj = array.optJSONObject(i);
                        Log.e("facebook", "objz" + obj);

                        String picture = obj.optString("picture");

                        Log.e("picture", "objz" + picture);
                        String message = obj.optString("message");
                        String created_time = obj.optString("created_time");
                        String id1 = obj.optString("id");
                        Log.e("message", "objz" + message);


                        if (obj.has("comments")) {
                            JSONObject comments = obj.optJSONObject("comments");
                            Log.e("comments", "objz" + comments);

                            JSONArray commentsdata = comments.optJSONArray("data");
                            Log.e("commentsdataSize", "objz" + commentsdata.length());
                            for (int k = 0; k < commentsdata.length(); k++) {
                                JSONObject obj2 = commentsdata.optJSONObject(k);

                                Log.e("commentsdata", "objz" + obj2);

                                String commentmessage = obj2.optString("message");

                                Log.e("commentmessage", "objz" + commentmessage);


                            }
                            commentCount = String.valueOf(commentsdata.length());
                            Log.e("commentCount", "objz" + commentCount);

                        }

                        JSONObject likes = obj.optJSONObject("likes");
                        Log.e("likes", "objz" + likes);

                        JSONArray data1 = likes.optJSONArray("data");

                        Log.e("json", "objz" + data1.length());

                        ArrayList<String> likesId = new ArrayList<String>();
                        for (int j = 0; j < data1.length(); j++) {  //loop  10;//id 5,3

                            JSONObject obj1 = data1.optJSONObject(j);

                            String dataid = obj1.optString("id");
                            Log.e("name", "objz" + dataid);

                            likesId.add(dataid);

                        }

                        String likesCount = String.valueOf(data1.length());

                        Log.e("likesCount", "objz" + likesCount + "  " + i);
                        String count = "" + i;


                        HashMap<String, String> map = new HashMap<>();


                        map.put(MyConstant.FACEBOOKMESSAGE, message);
                        map.put(MyConstant.FACEBOOKCOMMENTID, id1);
                        map.put(MyConstant.FACEBOOKPICTURE, picture);
                        map.put(MyConstant.FACEBOOKSLIKES, likesCount);
                        map.put(MyConstant.FACEBOOKCOMMENTSCOUNT, commentCount);
                        map.put(MyConstant.FACEBOOKCREATED_TIME, created_time);
                       // map.put(MyConstant.POSITION, count);
                        map.put(MyConstant.TYPE, MyConstant.FACEBOOK_USER);
                        map.put(MyConstant.FACEBOOKLIKEID, likesId.toString());

                        Log.e("true", "feedlist");
                        feedsList.add(map);
                        Log.e("facebookFeed", "list" + feedsList.toString());

                    }


                    setData();


                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));


    }


    private void getTwitterData(String twitterUserName) {
        MyUtil.execute(new Super_AsyncTask(context, twitterUserName, true, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {
                Log.e("object", "TWITTER" + output);
                loadmethod=3;

                try {
                    feedsList = new ArrayList<>();
                    // JSONObject object = new JSONObject(output);
                    Log.e("object", "TWITTER" + output);


                    JSONArray statuses = new JSONArray(output);//object.optJSONArray("statuses");
                    Log.e("statuses", "statuses" + statuses);
                    Log.e("SIZE", "LENGTH" + statuses.length());
                    for (int i = first; i <= loadsize; i++) {
                        media_url = null;
                        JSONObject obj = statuses.optJSONObject(i);
                        String created_at = obj.optString("created_at");
                        String text = obj.optString("text");

                        String favorite_count = obj.optString("favorite_count");
                        Log.e("text", "text" + created_at + " " + text);


                        if (obj.has("extended_entities")) {

                            JSONObject entities = obj.optJSONObject("extended_entities");
                            Log.e("twitter", "entities" + entities);

                            JSONArray media = entities.optJSONArray("media");


                            Log.e("media", "length" + media.length());

                            //     for (int j = 0; j <= 0; j++) {

                            JSONObject obj1 = media.optJSONObject(0);

                            Log.e("twitter", "obj1" + obj1);
                            media_url = obj1.optString("media_url");

                            Log.e("twitter", "profileImage" + media_url);
                            //  }
                        }


                        HashMap<String, String> map = new HashMap<>();
                        map.put(MyConstant.TWITTERTEXT, text);
                        map.put(MyConstant.TWITTERCREATED_TIME, created_at);
                        map.put(MyConstant.TWITTERPROFILEIMAGE, media_url);
                        map.put(MyConstant.TWITTERLIKES, favorite_count);
                        map.put(MyConstant.TYPE, MyConstant.TWITTER_USER);
                        feedsList.add(map);
                    }


                    // Log.e("feedlist","feedlist"+feedsList.toString());

                    setData();
                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }));


    }




    private void getTwitterComment(String twitterUserName) {
        MyUtil.execute(new Super_AsyncTask(context, twitterUserName, true, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {
                Log.e("comments", "TWITTER" + output);


              /*  try {
                    feedsList = new ArrayList<>();
                    // JSONObject object = new JSONObject(output);
                    Log.e("object", "TWITTER" + output);


                    JSONArray statuses = new JSONArray(output);//object.optJSONArray("statuses");
                    Log.e("statuses", "statuses" + statuses);
                    Log.e("SIZE", "LENGTH" + statuses.length());
                    for (int i = 0; i <= 17; i++) {
                        media_url = null;
                        JSONObject obj = statuses.optJSONObject(i);
                        String created_at = obj.optString("created_at");
                        String text = obj.optString("text");

                        String favorite_count = obj.optString("favorite_count");
                        Log.e("text", "text" + created_at + " " + text);


                        if (obj.has("extended_entities")) {

                            JSONObject entities = obj.optJSONObject("extended_entities");
                            Log.e("twitter", "entities" + entities);

                            JSONArray media = entities.optJSONArray("media");


                            Log.e("media", "length" + media.length());

                            //     for (int j = 0; j <= 0; j++) {

                            JSONObject obj1 = media.optJSONObject(0);

                            Log.e("twitter", "obj1" + obj1);
                            media_url = obj1.optString("media_url");

                            Log.e("twitter", "profileImage" + media_url);
                            //  }
                        }


                        HashMap<String, String> map = new HashMap<>();
                        map.put(MyConstant.TWITTERTEXT, text);
                        map.put(MyConstant.TWITTERCREATED_TIME, created_at);
                        map.put(MyConstant.TWITTERPROFILEIMAGE, media_url);
                        map.put(MyConstant.TWITTERLIKES, favorite_count);
                        map.put(MyConstant.TYPE, MyConstant.TWITTER_USER);
                        feedsList.add(map);
                    }


                    // Log.e("feedlist","feedlist"+feedsList.toString());

                    setData();
                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }*/
            }
        }));


    }








    @Override
    public void onStart() {
        super.onStart();


        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Feeds Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.brst_pc17.zumespot/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Feeds Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.brst_pc17.zumespot/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


}

//INSTA{"meta":{"code":200},"data":{"username":"eggslut","bio":"Eggslut Grand Central Market | Los Angeles \nEggslut Cosmopolitan | Las Vegas\n#eggslut #eggslutla #eggslutlv #eggsontheregs","website":"http:\/\/www.eggslut.com","profile_picture":"https:\/\/igcdn-photos-g-a.akamaihd.net\/hphotos-ak-xpt1\/t51.2885-19\/914352_650423288386038_1116598298_a.jpg","full_name":"eggslut","counts":{"media":510,"followed_by":65546,"follows":0},"id":"18790254"}}