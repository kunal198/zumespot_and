package com.example.brst_pc17.zumespot.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask_Interface;
import com.example.brst_pc17.zumespot.MyUtilities.ImageViewSmoothScroll;
import com.example.brst_pc17.zumespot.MyUtilities.MyConstant;
import com.example.brst_pc17.zumespot.MyUtilities.MyUtil;
import com.example.brst_pc17.zumespot.MyUtilities.StaggeredGridViewItem;
import com.example.brst_pc17.zumespot.R;
import com.example.brst_pc17.zumespot.WebViewActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sharan on 31/5/16.
 */
public class FeedsAdapter extends StaggeredGridViewItem {
    Context context;

    View mView;
    ImageView img_like;
    int mHeight;
    LinearLayout layout_postcomment;
    HashMap<String, String> item;
    private int position = 0;

    boolean liked, unliked;
    TextView txtv_like_count;
    TextView txtv_comment_count;
    MyUtil myUtil = new MyUtil();

    public FeedsAdapter(Context context, HashMap<String, String> item) {
        this.context = context;
        this.item = item;


    }


    @Override
    public View getView(LayoutInflater inflater, ViewGroup parent) {
        // LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//getFacebookLike


        Log.e("getType", "adapter" + item.get(MyConstant.FeedPicUrl));
        mView = inflater.inflate(R.layout.custom_feeds_item, parent, false);

        TextView text_view = (TextView) mView.findViewById(R.id.text_view);
        ImageViewSmoothScroll imgv_feed = (ImageViewSmoothScroll) mView.findViewById(R.id.imgv_feed);
        TextView txtv_caption = (TextView) mView.findViewById(R.id.txtv_caption);
        TextView txtv_date = (TextView) mView.findViewById(R.id.txtv_date);
        txtv_like_count = (TextView) mView.findViewById(R.id.txtv_like_count);
        txtv_comment_count = (TextView) mView.findViewById(R.id.txtv_comment_count);

        img_like = (ImageView) mView.findViewById(R.id.img_like);


        LinearLayout layout_comment = (LinearLayout) mView.findViewById(R.id.layout_comment);
        layout_postcomment = (LinearLayout) mView.findViewById(R.id.layout_postcomment);
        final EditText edit_comment = (EditText) mView.findViewById(R.id.edit_comment);
        Button button_post = (Button) mView.findViewById(R.id.button_post);


        Log.e("RECEIVEUSER", "FEEDSADAPTER" + item.get(MyConstant.UserHasLiked));
        if (item.get(MyConstant.TYPE).equals("InstagramUser")) {
            myUtil.setImage(context, imgv_feed, item.get(MyConstant.FeedPicUrl));

            txtv_caption.setText(item.get(MyConstant.FeedCaptionText));
            txtv_date.setText(myUtil.changeInstaDate(item.get(MyConstant.FeedTime)));
            txtv_like_count.setText(item.get(MyConstant.FeedLikeCount));
            txtv_comment_count.setText(item.get(MyConstant.FeedCommentCount));
            if(item.get(MyConstant.UserHasLiked).equals("true")){
                img_like.setImageResource(R.mipmap.ic_like_selected);
            }
            else if(item.get(MyConstant.UserHasLiked).equals("alse")){
                img_like.setImageResource(R.mipmap.ic_like_unselected);
            }

        } else if (item.get(MyConstant.TYPE).equals("FacebookUser")) {

            Log.e("facebookpicture", "receive" + item.get(MyConstant.FACEBOOKPICTURE));
            txtv_caption.setText(item.get(MyConstant.FACEBOOKMESSAGE));
            txtv_like_count.setText(item.get(MyConstant.FACEBOOKSLIKES));
            txtv_comment_count.setText(item.get(MyConstant.FACEBOOKCOMMENTSCOUNT));
            txtv_date.setText(item.get(MyConstant.FACEBOOKCREATED_TIME));
            // img_like.setTag(item.get(MyConstant.POSITION));

            if (item.get(MyConstant.FACEBOOKPICTURE).length() > 1) {
                myUtil.setImage(context, imgv_feed, item.get(MyConstant.FACEBOOKPICTURE));

            } else {
                text_view.setVisibility(View.VISIBLE);
                imgv_feed.setVisibility(View.GONE);
            }


            Log.e("resFacebookFeeds", "usersID" + item.get(MyConstant.FACEBOOKLIKEID));


            if (item.get(MyConstant.FACEBOOKLIKEID).contains(MyConstant.FACEBOOK_ID)) {
                Log.e("checkIF", "inIF");
                img_like.setImageResource(R.mipmap.ic_like_selected);
                liked = true;

                item.put(MyConstant.FACEBOOKPOSTLIKE, String.valueOf(liked));

            } else {
                Log.e("checkelse", "inelse");
                img_like.setImageResource(R.mipmap.ic_like_unselected);
                unliked = false;

                item.put(MyConstant.FACEBOOKPOSTUNLIKE, String.valueOf(unliked));

            }
        } else if (item.get(MyConstant.TYPE).equals("TwitterUser")) {


            myUtil.setImage(context, imgv_feed, item.get(MyConstant.TWITTERPROFILEIMAGE));
            txtv_caption.setText(item.get(MyConstant.TWITTERTEXT));
            Log.e("resIMAGETWITTER", "INSETADAPTER" + item.get(MyConstant.TWITTERPROFILEIMAGE));

            txtv_date.setText(item.get(MyConstant.TWITTERCREATED_TIME));
            txtv_like_count.setText(item.get(MyConstant.TWITTERLIKES));


        }
        /*mView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(context, WishDetails.class);
                i.putExtra("from_where", "Search_Initial");
                context.startActivity(i);
            }
        });*/


        layout_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layout_postcomment.setVisibility(View.VISIBLE);
            }
        });
        button_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (item.get(MyConstant.TYPE).equals("FacebookUser")) {
                    String commentId = item.get(MyConstant.FACEBOOKCOMMENTID);
                    Log.e("commentId", "FEED" + commentId);

                    String post = edit_comment.getText().toString();

                    getFacebokComments(commentId, post);
                } else if (item.get(MyConstant.TYPE).equals("InstagramUser")) {
                    String mediaId = item.get(MyConstant.FeedMediaId);

                    String post = edit_comment.getText().toString();
                    getInstagramComment(mediaId, post);
                }
//************************************************** INSTAGRAM COMMENT POST ****************************************************************************************//
               /* String mediaId=item.get(MyConstant.FeedMediaId);

                String post = edit_comment.getText().toString();
                getInstagramComment(mediaId,post);*/


//************************************************** FACEBOOK COMMENT POST ****************************************************************************************//

               /* String commentId = item.get(MyConstant.FACEBOOKCOMMENTID);
                Log.e("commentId", "FEED" + commentId);

                String post = edit_comment.getText().toString();

                getFacebokComments(commentId, post);*/
                // FEED198478933615527_723088634487885
//198478933615527_651560754974007
            }
        });


        Log.e("true", "true" + liked + ".." + unliked);


        img_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               /* Intent gotoWebView=new Intent(context, WebViewActivity.class);
                context.startActivity(gotoWebView);*/
                //getTwitterLike("785195614900477952");


                if (item.get(MyConstant.TYPE).equals("FacebookUser")) {
                    String commentId = item.get(MyConstant.FACEBOOKCOMMENTID);
                    Log.e("commentId", "FEED" + commentId + ".....CHECK" + item.get(MyConstant.FACEBOOKPOSTLIKE) + ".....CHECKed" + item.get(MyConstant.FACEBOOKPOSTUNLIKE));
                    getFacebookLike(commentId);
                } else if (item.get(MyConstant.TYPE).equals("InstagramUser")) {
                    String mediaId = item.get(MyConstant.FeedMediaId);
                    getInstagramLike(mediaId);
                }
//************************************************** INSTAGRAM LIKE ********************************************************************************************//

               /* String mediaId=item.get(MyConstant.FeedMediaId);
                getInstagramLike(mediaId);


                Log.e("resMEDIAID","feeds"+mediaId);*/


//************************************************** FACEBOOK LIKE UNLIKE ****************************************************************************************//
              /*  String commentId = item.get(MyConstant.FACEBOOKCOMMENTID);
                Log.e("commentId", "FEED" + commentId + ".....CHECK" + item.get(MyConstant.FACEBOOKPOSTLIKE) + ".....CHECKed" + item.get(MyConstant.FACEBOOKPOSTUNLIKE));
                //getFacebookLike(commentId);

                if (item.get(MyConstant.FACEBOOKPOSTLIKE) != null) {
                    getFacebookUnlike(commentId);

                } else if (item.get(MyConstant.FACEBOOKPOSTLIKE) == null) {
                    getFacebookLike(commentId);

                }*/
              /*  if(!item.get(MyConstant.FACEBOOKPOSTLIKE).equals(true)){
                    Log.e("ifLIKE", "UNLIKE");
                    getFacebookUnlike(commentId);
                }*/

             /* else  if(item.get(MyConstant.FACEBOOKPOSTUNLIKE).equals(null)){
                    Log.e("elseUNLIKE", "LIKE");
                    getFacebookLike(commentId);
                }*/

            }
        });

        return mView;
    }

    @Override
    public int getViewHeight(LayoutInflater inflater, ViewGroup parent) {
        LinearLayout item_containerFrameLayout = (LinearLayout) mView.findViewById(R.id.container);
        item_containerFrameLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        mHeight = item_containerFrameLayout.getMeasuredHeight();
        return mHeight;
    }


    public void facebookComment() {


        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "198478933615527_723088634487885",//"/{comment-id}",
                null,
                HttpMethod.POST,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {


                        Log.e("response", "FACEBOOK" + response);
            /* handle the result */
                    }
                }
        ).executeAsync();

    }

    public void getFacebokComments(String postId, String post) {

        Log.e("inInstafeeds", "service");
        String url = MyConstant.getFacebookComments(postId, post);
        Log.e("Index : ", url);

        HashMap<String, String> map = new HashMap<>();


        MyUtil.execute(new Super_AsyncTask(context, map, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                Log.e("output", "receivr" + output);

                Toast.makeText(context, "Successfully posted.", Toast.LENGTH_SHORT).show();

                layout_postcomment.setVisibility(View.GONE);


                if(txtv_comment_count.getText().toString().length()>0) {
                    int comment = Integer.parseInt(txtv_comment_count.getText().toString());
                    Log.e("getCount", "getCount" + comment);
                    int setCommentCount = comment + 1;
                    String setStrComment = String.valueOf(setCommentCount);


                    txtv_comment_count.setText(setStrComment);
                }

                else {
                    txtv_comment_count.setText("1");
                }

            }
        }, true));
    }


    public void getFacebookLike(String postId) {

        String url = MyConstant.facebookLike(postId);

        Log.e("postId", "postId" + postId);

        Log.e("postIdurl", "postIdurl" + url);
        Log.e("Index : ", url);

        HashMap<String, String> map = new HashMap<>();


        MyUtil.execute(new Super_AsyncTask(context, map, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                Log.e("output", "receivr" + output);

                if(txtv_like_count.getText().toString().length()>0) {
                    String getCount = txtv_like_count.getText().toString();

                    int count = Integer.parseInt(getCount);
                    int resCount = count + 1;
                    String setFbLike = String.valueOf(resCount);

                    Log.e("rescount", "set" + resCount);

                    txtv_like_count.setText(setFbLike);

                }
                else {
                    txtv_like_count.setText("1");
                }
                img_like.setImageResource(R.mipmap.ic_like_selected);


                try {

                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));

    }

    public void getFacebookUnlike(String postId) {

        String url = MyConstant.facebookUnlike(postId);

        Log.e("postId", "postId" + postId);

        Log.e("postIdurl", "postIdurl" + url);
        Log.e("Index : ", url);

        HashMap<String, String> map = new HashMap<>();


        MyUtil.execute(new Super_AsyncTask(context, map, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                Log.e("output", "receivr" + output);

                String getCount = txtv_like_count.getText().toString();
                int count = Integer.parseInt(getCount);
                int resCount = count - 1;

                Log.e("rescount", "set" + resCount);
                // txtv_like_count.setText(resCount);


                img_like.setImageResource(R.mipmap.ic_like_unselected);


                try {

                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));

    }


    public void getInstagramComment(String mediaID, String post) {

        String url = MyConstant.instagramComment(mediaID);

        Log.e("postId", "postId" + mediaID);

        Log.e("postIdurl", "postIdurl" + url);
        Log.e("Index : ", url);
        HashMap<String, String> map = new HashMap<>();
        map.put("access_token", "3021402478.42a1a72.9c4b624c4a064e7c811e658bc749237f");//1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b
        map.put("text", post);

//https://api.instagram.com/v1/media/1325360203683754053_3021402478/likes?access_token=1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b

        // String urll="https://api.instagram.com/v1/media/1325360203683754053_3021402478/comments?access_token=1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b&text=helloAll";

        MyUtil.execute(new Super_AsyncTask(context, map, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {


                Log.e("output", "receivr" + output);

                layout_postcomment.setVisibility(View.GONE);
                if(txtv_comment_count.getText().toString().length()>0) {
                    int comment = Integer.parseInt(txtv_comment_count.getText().toString());

                    int setCommentCount = comment + 1;
                    String setStrComment = String.valueOf(setCommentCount);


                    txtv_comment_count.setText(setStrComment);
                }
                else{
                    txtv_comment_count.setText("1");
                }
                try {

                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));

    }

    public void getInstagramLike(String mediaID) {

        String url = MyConstant.instagramLike(mediaID);

        Log.e("postId", "postId" + mediaID);

        Log.e("postIdurl", "postIdurl" + url);
        Log.e("Index : ", url);
        HashMap<String, String> map = new HashMap<>();
        map.put("access_token", "3021402478.42a1a72.9c4b624c4a064e7c811e658bc749237f");//1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b

//https://api.instagram.com/v1/media/1325360203683754053_3021402478/likes?access_token=1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b

        // String urll="https://api.instagram.com/v1/media/1325360203683754053_3021402478/comments?access_token=1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b&text=helloAll";

        MyUtil.execute(new Super_AsyncTask(context, map, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                if(txtv_like_count.getText().toString().length()>0) {
                    int like = Integer.parseInt(txtv_like_count.getText().toString());

                    int setLike = like + 1;
                    String setStrLike = String.valueOf(setLike);
                    Log.e("setLike", "setLike" + setLike);

                    Log.e("output", "receivr" + output);
                    txtv_like_count.setText(setStrLike);
                }

                else {
                    txtv_like_count.setText("1");
                }
                img_like.setImageResource(R.mipmap.ic_like_selected);

                layout_postcomment.setVisibility(View.GONE);

                try {

                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));

    }


    public void getTwitterLike(String tweetId) {


       /* TweetComposer.Builder builder = new TweetComposer.Builder(context)
                .text("just setting up my Fabric.")
                .image(Uri.parse("https://twitter.com/@bigmanbakes/profile_image?size=original"));
        builder.show();*/


        //String url = MyConstant.twitterLike(tweetId);
      /*  String url1 = "https://api.twitter.com/1.1/favorites/create.json?id=785904080690569216";

        //  Log.e("tweetId", "tweetId" + tweetId);

        // Log.e("postIdurl", "postIdurl" + url1);

        HashMap<String, String> map = new HashMap<>();


        MyUtil.execute(new Super_AsyncTask(context, url1, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                Log.e("output", "receivr" + output);


            }
        }, true));*/

    }


    public void getTwitterComment() {

       /* String url = MyConstant.instagramComment(mediaID);

        Log.e("postId", "postId" + mediaID);

        Log.e("postIdurl", "postIdurl" + url);
        Log.e("Index : ", url);
        HashMap<String,String> map=new HashMap<>();
        map.put("access_token","1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b");//1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b
        map.put("text",post);*/

     /*   String tweetUrl = "https://twitter.com/intent/tweet?text=hii&url="
                + "https://www.google.com";
*/
        String tweetUrl = "https://twitter.com/intent/tweet?text=hiii&urlhttps://www.google.com";


        Uri uri = Uri.parse(tweetUrl);

        Log.e("tweetUrl", "result" + tweetUrl);

        Log.e("URI", "result" + uri);
        context.startActivity(new Intent(Intent.ACTION_VIEW, uri));


//https://api.instagram.com/v1/media/1325360203683754053_3021402478/likes?access_token=1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b

        // String urll="https://api.instagram.com/v1/media/1325360203683754053_3021402478/comments?access_token=1507971356.42a1a72.b3641c02ff2d4028a1eefae9b643a41b&text=helloAll";

        MyUtil.execute(new Super_AsyncTask(context, tweetUrl, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {


                Log.e("output", "receivr" + output);

                layout_postcomment.setVisibility(View.GONE);

                try {

                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));

    }


}



