package com.example.brst_pc17.zumespot.Async_Thread;

import android.util.Base64;
import android.util.Log;

import com.example.brst_pc17.zumespot.MyUtilities.MyConstant;
import com.google.gson.Gson;
import okhttp3.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gagan on 8/18/15.
 */
public class WebServiceHandler
{

/*    public String performPostCall(String requestURL, HashMap<String, String> postDataParams)
    {

        URL url;
        String response = "";
        try
        {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //  conn.setReadTimeout(15000);
            //conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.addRequestProperty("Cache-Control", "no-cache");
            conn.addRequestProperty("Content-Type", "text/plain; charset=utf-8");

//            conn.setChunkedStreamingMode(1024);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK)
            {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                while ((line = br.readLine()) != null)
                {
                    response += line;
                }
            }
            else
            {
                response = "ERROR";

                // throw new Exception(responseCode + "");
            }
        }
      *//*  catch (Exception e)
        {
            response = "ERROR";
            e.printStackTrace();
        }*//*
        catch (SocketTimeoutException e)
        {
            e.printStackTrace();
            return "SLOW";
        }
        catch (ConnectTimeoutException e)
        {
            e.printStackTrace();
            return "SLOW";
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            return "ERROR";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "SLOW";
        }

        return response;
    }*/

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (first)
            {
                first = false;

            }
            else
            {
                result.append("&");

            }

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

   /* public String performGetCall(String url) throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();

        //print result

//        Log.e("==getCall response====", response.toString());
        return response.toString(); //this is your response

    }*/

    String res = "";

    public String getPostDataResponse(String url, HashMap<String, String> params) throws UnsupportedEncodingException
    {

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, getPostDataString(params));
        Request request = new Request.Builder().url(url).post(body).addHeader("cache-control", "no-cache").addHeader("content-type", "application/x-www-form-urlencoded").build();

        try
        {
            Response response = client.newCall(request).execute();

            return res = response.body().string();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return res;

    }

    public String getGetDataResponse(String URL) throws UnsupportedEncodingException
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url(URL).get().addHeader("cache-control", "no-cache").build();

//        Response response = client.newCall(request).execute();}


        try
        {
            Response response = client.newCall(request).execute();

            return res = response.body().string();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return res;
    }


    //*****************************************************************************************************
    //****************************************   Twitter   ************************************************
    //*****************************************************************************************************


    private String getResponseBody(HttpRequestBase request)
    {
        StringBuilder sb = new StringBuilder();
        try
        {

            DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            String reason = response.getStatusLine().getReasonPhrase();

            if (statusCode == 200)
            {

                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();

                BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                String line = null;
                while ((line = bReader.readLine()) != null)
                {
                    sb.append(line);
                }
            }
            else
            {
                sb.append(reason);
            }
        }
        catch (UnsupportedEncodingException ex)
        {
        }
        catch (ClientProtocolException ex1)
        {
        }
        catch (IOException ex2)
        {
        }
        return sb.toString();
    }


    public String getTwitterStream(String screenName)
    {
        String results = null;

        // Step 1: Encode consumer key and secret
        try
        {


            Log.e("try","intwitterdata");
            // URL encode the consumer key and secret
            String urlApiKey = URLEncoder.encode(MyConstant.CONSUMER_KEY, "UTF-8");
            String urlApiSecret = URLEncoder.encode(MyConstant.CONSUMER_SECRET, "UTF-8");

            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;

            // Base64 encode the string
            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
            Log.e("try","intwitterdata"+base64Encoded);

            // Step 2: Obtain a bearer token
            HttpPost httpPost = new HttpPost(MyConstant.TwitterTokenURL);
            Log.e("try","intwitterdata"+MyConstant.TwitterTokenURL);

            httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new StringEntity("grant_type=client_credentials"));

            String rawAuthorization = getResponseBody(httpPost);

            Authenticated auth = jsonToAuthenticated(rawAuthorization);

            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer"))
            {
                Log.e("inbearer","IF");

                // Step 3: Authenticate API requests with bearer token
                HttpGet httpGet = new HttpGet(MyConstant.TwitterStreamURL + screenName);
                Log.e("BALLIDAKU","httpget"+httpGet+MyConstant.TwitterStreamURLL+screenName);
                // construct a normal HTTPS request and include an Authorization
                // header with the value of Bearer <>
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                // update the results with the body of the response
                results = getResponseBody(httpGet);
            }
        }
        catch (UnsupportedEncodingException ex)
        {
        }
        catch (IllegalStateException ex1)
        {
        }
        return results;
    }


    // convert a JSON authentication object into an Authenticated object
    private Authenticated jsonToAuthenticated(String rawAuthorization)
    {
        Authenticated auth = null;
        if (rawAuthorization != null && rawAuthorization.length() > 0)
        {
            try
            {
                Gson gson = new Gson();
                auth = gson.fromJson(rawAuthorization, Authenticated.class);
            }
            catch (IllegalStateException ex)
            {
                // just eat the exception
            }
        }
        return auth;
    }

    public class Authenticated {
        String token_type;
        String access_token;
    }

    public String copy_getTwitterStream(String screenName) {
        String results = null;

        // Step 1: Encode consumer key and secret
        try {


            Log.e("try", "intwitterdata");
            // URL encode the consumer key and secret
            // Step 2: Obtain a bearer token
            HttpPost httpPost = new HttpPost(MyConstant.TwitterTokenURL_1);
            Log.e("try", "intwitterdata" + MyConstant.TwitterTokenURL);

            // httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/json; charset=utf-8");
            httpPost.setHeader("Content-Length", "127");

            String rawAuthorization = getResponseBody(httpPost);

            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("inbearer", "auth " + auth.toString());

            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer")) {
                Log.e("inbearer", "auth111 " + auth.toString());

                // Step 3: Authenticate API requests with bearer token
                HttpPost httpGet = new HttpPost(MyConstant.TwitterStreamURL1);
                Log.e("BALLIDAKU", "httpget" + httpGet + MyConstant.TwitterStreamURLL + screenName);
                // construct a normal HTTPS request and include an Authorization
                // header with the value of Bearer <>
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                results = getResponseBody(httpGet);
            }
        } catch (Exception ex) {
        }
        return results;
    }
}