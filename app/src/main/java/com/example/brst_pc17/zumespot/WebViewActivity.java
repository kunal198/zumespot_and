package com.example.brst_pc17.zumespot;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Created by brst-pc89 on 10/14/16.
 */
public class WebViewActivity extends Activity {

    WebView webV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        webV = (WebView) findViewById(R.id.id_web_view);
        webV.loadUrl("https://twitter.com/intent/like?tweet_id=786395630151331840");
    }
}
