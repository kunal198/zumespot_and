package com.example.brst_pc17.zumespot.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc17.zumespot.FeedsActivity;
import com.example.brst_pc17.zumespot.MyUtilities.MyConstant;
import com.example.brst_pc17.zumespot.MyUtilities.MyUtil;
import com.example.brst_pc17.zumespot.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc17 on 8/27/16.
 */
public class MainActivityAdapter extends BaseAdapter {
    Context context;

    MyUtil myUtil = new MyUtil();

    ArrayList<HashMap<String, String>> jsonList;

    ArrayList<HashMap<String, String>> list;

    String showValue, newVal;
    int mul, mult;

    public MainActivityAdapter(Context context, ArrayList<HashMap<String, String>> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub


        Log.e("list", "SIZE" + list.size());
        return list.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int position, View row, ViewGroup parent) {
        LayoutInflater inflater = null;
        Viewholder viewholder = null;
        // if(row==null) {
        String type=null;
        if (row == null) {
            viewholder = new Viewholder();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.testing, parent, false);
            viewholder.imgv_users = (ImageView) row.findViewById(R.id.imgv_users);
            viewholder.title = (TextView) row.findViewById(R.id.title);
            viewholder.distance = (TextView) row.findViewById(R.id.distance);
             type = list.get(position).get(MyConstant.TYPE);

            Log.e("received","imgurl"+list.get(position).get(MyConstant.InstagramPicUrl));


            row.setTag(viewholder);


        } else {
            viewholder = (Viewholder) row.getTag();
        }

        Log.e("received","imgurl"+list.get(position).get(MyConstant.InstagramPicUrl));

        if (list.get(position).get(MyConstant.TYPE).equals("InstagramUser")) {
            String imgUrl = list.get(position).get(MyConstant.InstagramPicUrl);
            myUtil.setImageUrl(context, viewholder.imgv_users, imgUrl);

            Log.e("instaImage","url"+list.get(position).get(MyConstant.InstagramPicUrl));

            viewholder.title.setText(list.get(position).get(MyConstant.InstagramFullName) + " " + MyConstant.INSTAGRAM_USER);
            mul = Integer.parseInt(list.get(position).get(MyConstant.InstagramDistance));

            newVal = String.valueOf(mul * 0.00056818);


            viewholder.distance.setText(String.format("%.2f", Double.parseDouble(newVal)) + " Miles");

            viewholder.title.setSelected(true);

        } else if (list.get(position).get(MyConstant.TYPE).equals("FacebookUser")) {

            String imgUrl = list.get(position).get(MyConstant.FacewbookPicUrl);

            Log.e("adapter", "imgurl" + imgUrl + " " + type + "pos " + position);
            myUtil.setImageUrl(context, viewholder.imgv_users, imgUrl);


            if (list.get(position).get(MyConstant.FacebookUserName).contains("@")) {
                String[] separated = list.get(position).get(MyConstant.FacebookUserName).split("@");
                separated[1] = separated[1].trim();
                viewholder.title.setText(separated[1] + " " + MyConstant.FACEBOOK_USER);
            } else {
                viewholder.title.setText(list.get(position).get(MyConstant.FacebookUserName)+ " " + MyConstant.FACEBOOK_USER);

            }
            newVal = String.valueOf(mul * 0.00056818);
            viewholder.distance.setText(String.format("%.2f", Double.parseDouble(newVal)) + " Miles");
            viewholder.title.setSelected(true);
        } else if (list.get(position).get(MyConstant.TYPE).equals("TwitterUser")) {
            String imgUrl = list.get(position).get(MyConstant.TwitterPicUrl);


            myUtil.setImageUrl(context, viewholder.imgv_users, imgUrl);
            if (list.get(position).get(MyConstant.TwitterUserName).contains("@")) {
                String[] separated = list.get(position).get(MyConstant.TwitterUserName).split("@");
                separated[1] = separated[1].trim();
                viewholder.title.setText(separated[1] + " " + MyConstant.TWITTER_USER);
            } else {
                viewholder.title.setText(list.get(position).get(MyConstant.TwitterUserName)+ " " + MyConstant.TWITTER_USER);

            }
            newVal = String.valueOf(mul * 0.00056818);
            viewholder.distance.setText(String.format("%.2f", Double.parseDouble(newVal)) + " Miles");

        }


        Log.e("RESFACEBOOKUSERNAME", "" + list.get(position).get(MyConstant.FacewbookPicUrl));



        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Clicked" +list.get(position).get(MyConstant.TYPE),Toast.LENGTH_SHORT).show();

                Intent i = new Intent(context, FeedsActivity.class);

                if(list.get(position).get(MyConstant.TYPE).equals("InstagramUser")) {

                 //   Log.e("instagramuserIDSEND","facebookuser"+list.get(position).get(MyConstant.InstagramId));
                    i.putExtra(MyConstant.InstagramFullName, list.get(position).get(MyConstant.InstagramFullName));
                    i.putExtra(MyConstant.InstagramId, list.get(position).get(MyConstant.InstagramId));
                    //i.putExtra(MyConstant.InstagramFullName, list.get(position).get(MyConstant.InstagramFullName));
                 }
                else if(list.get(position).get(MyConstant.TYPE).equals("FacebookUser")){
                    Log.e("FacebookUser","FacebookUser");

                    i.putExtra(MyConstant.FacebookUserName, list.get(position).get(MyConstant.FacebookUserName));
                    Log.e("MainUserId", "" + list.get(position).get(MyConstant.FacebookUserName));

                }
                else if(list.get(position).get(MyConstant.TYPE).equals("TwitterUser")){
                    Log.e("TwitterUser","TwitterUserNAME"+list.get(position).get(MyConstant.TwitterUserName));
                    i.putExtra(MyConstant.TwitterUserName, list.get(position).get(MyConstant.TwitterUserName)); //"@wexlersdeli");


                }
                context.startActivity(i);
            }
        });


        return row;
    }

    public static class Viewholder {
        ImageView imgv_users;
        TextView title, distance;
    }

    public void addData(ArrayList<HashMap<String, String>> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
