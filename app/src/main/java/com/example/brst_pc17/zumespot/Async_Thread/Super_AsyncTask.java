package com.example.brst_pc17.zumespot.Async_Thread;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.brst_pc17.zumespot.Login;
import com.example.brst_pc17.zumespot.MyUtilities.MyUtil;
import com.example.brst_pc17.zumespot.R;

import java.util.HashMap;

/**
 * Created by sharan on 10/9/15.
 */
public class Super_AsyncTask extends AsyncTask<Void, Void, String>
{

    String URL = "";
    String twitterUserName;

    HashMap<String, String> inputData = null;

    Context context;

    ProgressDialog dialog;
    Super_AsyncTask_Interface listener = null;
    boolean show_progressbar_or_not = false;
    MyUtil myUtil = new MyUtil();

    public Super_AsyncTask(Context context, HashMap<String, String> inputData, String URL, Super_AsyncTask_Interface listener, boolean show_progressbar_or_not)
    {
        this.context = context;
        this.inputData = inputData;
        this.URL = URL;
        this.listener = listener;
        this.show_progressbar_or_not = show_progressbar_or_not;

        myUtil.hide_keyboard(context);
    }

    public Super_AsyncTask(Context context, String URL, Super_AsyncTask_Interface listener, boolean show_progressbar_or_not)
    {
        this.context = context;
        this.URL = URL;
        this.listener = listener;
        this.show_progressbar_or_not = show_progressbar_or_not;
        myUtil.hide_keyboard(context);
    }

    /* For Twitter Only*/
    public Super_AsyncTask(Context context, String userName, boolean show_progressbar_or_not, Super_AsyncTask_Interface listener)
    {
        this.context = context;
        twitterUserName = userName;
        this.listener = listener;
        this.show_progressbar_or_not = show_progressbar_or_not;
        myUtil.hide_keyboard(context);
    }


  /*  public void cancelAsync()
    {
        if (super_asyncTask!=null)
        {
            super_asyncTask.cancel(true);
        }

    }*/

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();

        if (show_progressbar_or_not == true)
        {
            dialog = ProgressDialog.show(context, "", "");
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.progress_dialog);
            dialog.show();
        }

    }

    @Override
    protected String doInBackground(Void... params)
    {
        String response = "";

        Log.e("inputData", "" + inputData);
        try
        {

            if (!URL.isEmpty() && inputData != null)
            {
                response = new WebServiceHandler().getPostDataResponse(URL, inputData);
            }
            else if (!URL.isEmpty())
            {
//                response = new WebServiceHandler().performGetCall(URL);
                response = new WebServiceHandler().getGetDataResponse(URL);
            }
            else if (URL.isEmpty()) // Twitter Case
            {
                response = new WebServiceHandler().getTwitterStream(twitterUserName);

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String ResponseString)
    {
        super.onPostExecute(ResponseString);

        if (show_progressbar_or_not == true)
        {

            if (dialog.isShowing())
            {
                dialog.dismiss();
            }
        }

        Log.e("Response for " + context.getClass().getName(), " " + ResponseString);

        if (!ResponseString.equals("SLOW") && !ResponseString.equals("ERROR"))
        {
            listener.onTaskCompleted(ResponseString);

        }
        else if (ResponseString.equals("SLOW"))
        {
            MyUtil.show_Toast(context, "Please check your network.");
        }
        else if (ResponseString.equals("ERROR"))
        {
            MyUtil.show_Toast(context, "Server side error.");
        }

    }
}
