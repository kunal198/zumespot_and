package com.example.brst_pc17.zumespot;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask_Interface;
import com.example.brst_pc17.zumespot.MyUtilities.InstagramApp;
import com.example.brst_pc17.zumespot.MyUtilities.MyConstant;
import com.example.brst_pc17.zumespot.MyUtilities.MyUtil;
import com.facebook.*;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.*;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;
import okhttp3.Request;
import okhttp3.Response;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

public class Login extends AppCompatActivity implements View.OnClickListener {

    Context context;
    MyUtil myUtil = new MyUtil();

    /*Facebook*/ LoginButton login_button_fb;
    CallbackManager callbackManager;
    ProfileTracker profileTracker;


    /*Instagram*/ InstagramApp mApp;
    HashMap<String, String> userInfoHashmap = new HashMap<String, String>();


    /*Twitter*/
    private TwitterLoginButton loginButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //************************************************Facebook**************************************
        //goToNextScreen();
        FacebookSdk.sdkInitialize(getApplicationContext());

        //faceData();

        callbackManager = CallbackManager.Factory.create();
        if (BuildConfig.DEBUG) {

            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
        //**********************************************************************************************
        //********************************************** Twitter **************************************

        TwitterAuthConfig authConfig = new TwitterAuthConfig(MyConstant.TWITTER_KEY, MyConstant.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));


        //**********************************************************************************************


        setContentView(R.layout.activity_login);

        context = this;

        setUpIds();
        //getFacebookData("402093683256266");
        // getTwitterData("@BadmaashLA");
        get_fb_data();


        getTwitterData("@wexlersdeli");

//        myUtil.getKeyHash(con);


        //************************************************Facebook**************************************

        isAlreadyLogin(isLoggedIn());

        // Callback registration
        login_button_fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Log.e("fbresult", "result" + loginResult);

                getFacebookData("402093683256266");

                //faceData();


                facebookLike();
                 goToNextScreen();
//                get_fb_data();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        //**********************************************************************************************
        //*********************************************** Insatagram *************************************


        mApp = new InstagramApp(this, MyConstant.INSTAGRAM_CLIENT_ID, MyConstant.INSTAGRAM_CLIENT_SECRET, MyConstant.INSTAGRAM_CALLBACK_URL);

        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
                // tvSummary.setText("Connected as " + mApp.getUserName());
//                btnConnect.setText("Disconnect");
//                llAfterLoginView.setVisibility(View.VISIBLE);
                // userInfoHashmap = mApp.
//                mApp.fetchUserName();

                goToNextScreen();
            }

            @Override
            public void onFail(String error) {
                MyUtil.show_Toast(context, error);
            }
        });

        //**********************************************************************************************
        //**************************************** Twitter *********************************************


        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                goToNextScreen();

                // The TwitterSession is also available through:
                // Twitter.getInstance().core.getSessionManager().getActiveSession()
                TwitterSession session = result.data;
                // TODO: Remove toast and use the TwitterSession's userID
                // with your app's user model


                Log.e("twitterData", "userID" + session.getUserId());
                String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                //      Log.d("TwitterKit", "Login with Twitter failure", exception);

            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });


    }

    private void setUpIds() {
        findViewById(R.id.imgv_facebook).setOnClickListener(this);
        findViewById(R.id.imgv_twitter).setOnClickListener(this);
        findViewById(R.id.imgv_instagram).setOnClickListener(this);

        // Facebook
        login_button_fb = (LoginButton) findViewById(R.id.login_button_fb);
        login_button_fb.setReadPermissions(Arrays.asList("user_status, email"));



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.imgv_facebook:

                login_button_fb.performClick();

                getFacebookData("113635421999522");

                break;

            case R.id.imgv_twitter:

//                startActivity(new Intent(context,MainActivity.class));

//                getTwitterData("LasPerlasLA");

                loginButton.performClick();


                break;


            case R.id.imgv_instagram:

//                getInstagramData();

                connectOrDisconnectUser();


                break;

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("sharan :" + requestCode, "" + resultCode);

        if (requestCode == 64206) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == 140) {
            loginButton.onActivityResult(requestCode, resultCode, data);
        }


    }


    //**************************************************************************************************************
    //************************************************** Facebook **************************************************
    //**************************************************************************************************************

    public void get_fb_data() {
        try {
            final Profile profile = Profile.getCurrentProfile();

            Log.e("onSuccess", "4" + profile.getName());
            Log.e("onSuccess", "5" + profile.getId());
            Log.e("onSuccess", "6" + profile.getProfilePictureUri(500, 500));

            URL img_value = new URL(profile.getProfilePictureUri(500, 500).toString());


            login_register_through_fb(profile);

        } catch (Exception e) {
            e.printStackTrace();

            profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                    Log.e("AAAAAAAAAAAAAA", "Hello");

                    if (Profile.getCurrentProfile() != null) {
                        // Log.e("currentProfile", "Hello" + currentProfile.getName() + "...." + currentProfile.getProfilePictureUri(100, 100) + "...." + currentProfile.getId());

                        Log.e("onSuccess", "7" + currentProfile.getName());
                        Log.e("onSuccess", "8" + currentProfile.getId());
                        Log.e("onSuccess", "9" + currentProfile.getProfilePictureUri(500, 500).toString());

                        login_register_through_fb(currentProfile);

                    } else {
//                        stop_fb();
                    }
                }
            };

            profileTracker.startTracking();
        }
    }


    private void login_register_through_fb(Profile profile) {
//        HashMap<String, String> map = new HashMap<>();

        // Log.e("location.getLatitude()",""+location.getLatitude());

        String profileId = profile.getId();

        MyConstant.FACEBOOK_ID = profileId;
        Log.e("profile.getId()", "" + profile.getId());
        Log.e("getName", "" + profile.getName());
        Log.e("profile", "" + profile.toString());

        /*map.put(GlobalConstant.KeyValues_Names.EmailID.toString(), profile.getId());
        map.put(GlobalConstant.KeyValues_Names.Password.toString(), "");
        map.put(GlobalConstant.KeyValues_Names.ApplicationID.toString(), sp.getString("GCM_Reg_id", ""));
        map.put(GlobalConstant.KeyValues_Names.Latitude.toString(), "" + location.getLatitude());
        map.put(GlobalConstant.KeyValues_Names.Longitude.toString(), "" + location.getLongitude());
        map.put(GlobalConstant.KeyValues_Names.DeviceSerialNo.toString(), constant.get_Mac_Address(con));
        map.put(GlobalConstant.KeyValues_Names.DeviceType.toString(), "android");
        map.put(GlobalConstant.KeyValues_Names.Flag.toString(), "facebook");
        if (isLoginScreen)
        {
            LOGIN_SERVICE(map, view);
        }
        else
        {
            map.put(GlobalConstant.KeyValues_Names.FirstName.toString(), profile.getName());
            map.put(GlobalConstant.KeyValues_Names.UserName.toString(), profile.getId());
            SIGNUP_SERVICE(map, view);
        }*/
    }

    public boolean isLoggedIn() {
        Log.e("visit", "visit");
        AccessToken accessToken = AccessToken.getCurrentAccessToken();


        Log.e("accessToken", "visit" + accessToken);
        Log.e("accessToken", "visit" + AccessToken.getCurrentAccessToken());
        return accessToken != null;
    }


    private void isAlreadyLogin(boolean currentAccessToken) {

        Log.e("visit", "isalreadylogin");
        Log.e("current", "accesstoken" + currentAccessToken);
        if (currentAccessToken) {
            goToNextScreen();

//            stop_fb();
        }

    }

    public void stop_fb() {
        Log.e("Logout", "Logout");

        try {
            if (profileTracker.isTracking()) {
                profileTracker.stopTracking();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            AccessToken.setCurrentAccessToken(null);
            Profile.setCurrentProfile(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //***************************************************** Login service  *************************************************************************

    public void hit(HashMap<String, String> map, final View v) {



       /* Constant_Class.execute(new Super_AsyncTask(con, map, Constant_Class.base_url + "Customer/ValidateUserCustomer", new Super_AsyncTask_Interface()
        {
            @Override
            public void onTaskCompleted(String output)
            {
                try
                {
                    JSONObject object = new JSONObject(output);
                    if (object.getString("Status").equals("success"))
                    {
                        Constant_Class.SAVE_PERSONNEL_DATA(con, object.getString("Message"), Constant_Class.KeyValues_Names.Own_Data.toString());
                        startActivity(new Intent(con, MainActivity.class));
                        finish();
                        //                        startActivityForResult(new Intent(con, MainActivity.class), 3);
                    }
                    else
                    {
                        Constant_Class.show_snackbar(v, object.getString("Message"));
                    }
                }
                catch (Exception ex)
                {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));*/
    }


    //*********************************************************************************************************************
    //*********************************************************************************************************************
    //*********************************************************************************************************************

    public void getFacebookData(String pageID) {

        Log.e("visit", "inmain");

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/" + pageID + "/posts", null, HttpMethod.GET, new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                Log.e("Response", "inmain" + AccessToken.getCurrentAccessToken());

                Log.e("Response", "facebook" + response.toString());
            }
        }).executeAsync();

    }


    //*********************************************************************************************************************
    //************************************************** Twitter **********************************************************
    //*********************************************************************************************************************


//EAAX1cYei8GABAN6GhRuF42ChJ6eyK76PGvs8UfbXMmEwEhzqZBhJfzKRQ9Ycw6l8jt184j9GkAmc12Tz7SPheIOgwRxPAc06CVrPZBWz6Sk6t0PPWU847XAMFPtuWpI4JFeGKETxw1po3a1AWcX6vVnTW2UDOvZBoaEHrBwuGwzaxcYLquQypCSmznpBxIOQsyzBT6fgqf47W1bqnTW


    private void getTwitterData(String twitterUserName) {
        MyUtil.execute(new Super_AsyncTask(context, twitterUserName, true, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {
                Log.e("BALLIDAKU", "TWITTERS" + output);

                try {
                    //JSONObject object = new JSONObject(output);


                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }));


    }


    private void getTwittersdetails(String twitterUserName) {
        MyUtil.execute(new Super_AsyncTask(context, twitterUserName, true, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {
                Log.e("BALLIDAKU", "TWITTERS" + output);

                try {
                    //JSONObject object = new JSONObject(output);


                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }));


    }



    /*private void twittersData(){


        TwitterAuthConfig authConfig = new TwitterAuthConfig(MyConstant.TWITTER_KEY, MyConstant.TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                Log.e("TWITTWERRESULT","FORFEEDS"+result);
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
                Log.e("exception","FORFEEDS"+exception);
            }
        });
    }*/


    //*********************************************************************************************************************
    //**************************************************** Instagram  *****************************************************
    //*********************************************************************************************************************


    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {
//                userInfoHashmap = mApp.getUserInfo();
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                MyUtil.show_Toast(context, "Check your network.");
            }
            return false;
        }
    });

    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
            builder.setMessage("Disconnect from Instagram?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mApp.resetAccessToken();
// btnConnect.setVisibility(View.VISIBLE);
//                    llAfterLoginView.setVisibility(View.GONE);
//                    btnConnect.setText("Connect");
// tvSummary.setText("Not connected");
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            mApp.authorize();
        }
    }

    /*private void displayInfoDialogView()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Login.this);
        alertDialog.setTitle("Profile Info");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.profile_view, null);
        alertDialog.setView(view);
        ImageView ivProfile = (ImageView) view.findViewById(R.id.ivProfileImage);
        TextView tvName = (TextView) view.findViewById(R.id.tvUserName);
        TextView tvNoOfFollwers = (TextView) view.findViewById(R.id.tvNoOfFollowers);
        TextView tvNoOfFollowing = (TextView) view.findViewById(R.id.tvNoOfFollowing);
        new ImageLoader(MainActivity.this).DisplayImage(userInfoHashmap.get(InstagramApp.TAG_PROFILE_PICTURE), ivProfile);
        tvName.setText(userInfoHashmap.get(InstagramApp.TAG_USERNAME));
        tvNoOfFollowing.setText(userInfoHashmap.get(InstagramApp.TAG_FOLLOWS));
        tvNoOfFollwers.setText(userInfoHashmap.get(InstagramApp.TAG_FOLLOWED_BY));
        alertDialog.create().show();
    }*/

    public void getInstagramData() {

        MyUtil.execute(new Super_AsyncTask(context, MyConstant.GET_DATA_USING_USERNAME, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {
                try {

                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, true));
    }


    public void goToNextScreen() {
        startActivity(new Intent(context, MainActivity.class));
        finish();
    }


    public void faceData() {
        Log.e("newFB", "response");


        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        Log.e("acces", "tokenfb" + accessToken);

        new GraphRequest(
                accessToken.getCurrentAccessToken(),
                "/me/posts",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {

                        Log.e("newFB", "response" + response);
                        goToNextScreen();

                    }
                }
        ).executeAsync();
    }


    public void facebookLike() {

        /*new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            Log.e("NEW","FBres"+response);


                    }
                }
        ).executeAsync();*/
//        Request likeRequest = new Request(Session.getActiveSession(), fBPostId + "/likes", null, HttpMethod.POST, new Request.Callback() {
//
//            @Override
//            public void onCompleted(Response response) {
//                Log.i(TAG, response.toString());
//            }
//        });
//        Request.executeBatchAndWait(likeRequest);
    }











   /* public void facebookLike(){
        Request likeRequest = new Request(Session.getActiveSession(), fBPostId + "/likes", null, HttpMethod.POST, new Request.Callback() {

            //@Override
            public void onCompleted(Response response) {
                Log.i(TAG, response.toString());
            }
        });
        Request.executeBatchAndWait(likeRequest);
    }*/


}

    /*public String copy_getTwitterStream(String screenName) {
        String results = null;

        // Step 1: Encode consumer key and secret
        try {


            Log.e("try", "intwitterdata");
            // URL encode the consumer key and secret
            // Step 2: Obtain a bearer token
            HttpPost httpPost = new HttpPost(MyConstant.TwitterTokenURL_1);
            Log.e("try", "intwitterdata" + MyConstant.TwitterTokenURL);

            // httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/json; charset=utf-8");
            httpPost.setHeader("Content-Length", "127");

            String rawAuthorization = getResponseBody(httpPost);

            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("inbearer", "auth " + auth.toString());

            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer")) {
                Log.e("inbearer", "auth111 " + auth.toString());

                // Step 3: Authenticate API requests with bearer token
                HttpPost httpGet = new HttpPost(MyConstant.TwitterStreamURL1);
                Log.e("BALLIDAKU", "httpget" + httpGet + MyConstant.TwitterStreamURLL + screenName);
                // construct a normal HTTPS request and include an Authorization
                // header with the value of Bearer <>
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                results = getResponseBody(httpGet);
            }
        } catch (Exception ex) {
        }
        return results;
    }*/