package com.example.brst_pc17.zumespot;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.example.brst_pc17.zumespot.Adapters.MainActivityAdapter;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask;
import com.example.brst_pc17.zumespot.Async_Thread.Super_AsyncTask_Interface;
import com.example.brst_pc17.zumespot.MyUtilities.MyConstant;
import com.example.brst_pc17.zumespot.MyUtilities.MyUtil;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {
    Context context;
    GridView gridView;
    MainActivityAdapter mainActivityAdapter;
    int get_value = 6;
    ArrayList<HashMap<String, String>> jsonList;
    ArrayList<HashMap<String, String>> testList;
    ArrayList<HashMap<String, String>> mainList;
    ArrayList<String> lats, longs;
    ArrayList<Integer> arrDistance;
    ArrayList<Double> arrSortDistance;
    Location location;
    double latitude, longitude;
    ImageView load_more;
    private int count = 8, total_count, rem_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
// latitude: 34.050977, longitude: -118.248027)


/*       float    medium_grand_size = getResources().getDimension(R.dimen.image_size) / getResources().getDisplayMetrics().density;
        Log.e("SCREEN","RES"+medium_grand_size);*/

        Log.e("RES", "USERFACEBOOKID" + MyConstant.FACEBOOK_ID);
        context = this;
        lats = new ArrayList<>();
        longs = new ArrayList<>();
        testList = new ArrayList<>();
        arrDistance = new ArrayList<>();
        arrSortDistance = new ArrayList<>();
        load_more = (ImageView) findViewById(R.id.load_more);
        setUpIds();

        Fabric.with(this, new Crashlytics());

//        MyUtil.get_location(context);


        getJsonData();


        //testData();
        load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_value = get_value + 21;
                rotateOrNot();
            }
        });

    }


    private void setUpIds() {
        gridView = (GridView) findViewById(R.id.gridView);

    }

    private void setData() {

        Log.e("in", "setData");
        if (mainList.size() == 1) {
            mainActivityAdapter = new MainActivityAdapter(context, mainList);

            gridView.setAdapter(mainActivityAdapter);

        } else if (mainList.size() > 1) {


            int visible_size = 51;


            if (mainList.size() <= visible_size) {
                Log.e("MAIN20", "LISTSIZE" + mainList.size());
                mainActivityAdapter.addData(mainList);

            }


        }
        if (mainList.size() == 6) {
            load_more.setVisibility(View.VISIBLE);
        }


        rotateOrNot();

    }

    int positionMain = 0;

    private void rotateOrNot() {

        if (positionMain < get_value) {
            if (positionMain < jsonList.size()) {

                if (jsonList.get(positionMain).get(MyConstant.TYPE).equals("InstagramUser")) {
                    getInstagramData(positionMain);
                } else if (jsonList.get(positionMain).get(MyConstant.TYPE).equals("FacebookUser")) {
                    getFacebookData(positionMain);
                } else if (jsonList.get(positionMain).get(MyConstant.TYPE).equals("TwitterUser")) {
                    getTwitterData(positionMain);
                }

                positionMain++;

            }
        }
    }


    //***********************************************************************************************************************************

    public void getJsonData() {
        try {
            InputStream is = context.getAssets().open("social.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String bufferString = new String(buffer);

            jsonList = new ArrayList<>();
            mainList = new ArrayList<>();


            JSONArray jsonArray = new JSONArray(bufferString);


            Location current = new Location("");
            current.setLatitude(Double.parseDouble("34.050977"));//30.7021 and 76.8    34.050977   -118.248027
            current.setLongitude(Double.parseDouble("-118.248027"));
            Log.e("PUT", "jsonList1: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);

                // Instagram User *//**//*
                if (!obj.optString(MyConstant.JsonInstagramUserName).isEmpty() && !obj.optString(MyConstant.JsonInstagramUserId).equals("null")) {

                    HashMap<String, String> map = new HashMap<>();
                    map.put(MyConstant.JsonName, obj.optString(MyConstant.JsonName));
                    map.put(MyConstant.JsonInstagramUserName, obj.optString(MyConstant.JsonInstagramUserName));
                    map.put(MyConstant.JsonInstagramUserId, obj.optString(MyConstant.JsonInstagramUserId));

                    Location pi = new Location("");
                    pi.setLatitude(Double.parseDouble(obj.optString("Latitude")));
                    pi.setLongitude(Double.parseDouble(obj.optString("Longitude")));
                    long distance = (long) pi.distanceTo(current);
                    map.put(MyConstant.InstagramDistance, "" + distance);
                    Log.e("MyConstant.IN", "" + MyConstant.INSTAGRAM_USER);
                    map.put(MyConstant.TYPE, MyConstant.INSTAGRAM_USER);

                    jsonList.add(map);

                }

                if (!obj.optString(MyConstant.FacebookUserName).isEmpty() && !obj.optString(MyConstant.FacebookId).equals("null")) {
                    HashMap<String, String> map = new HashMap<>();


                    map.put(MyConstant.FacebookId, obj.optString(MyConstant.FacebookId));

                    map.put(MyConstant.FacebookUserName, obj.optString(MyConstant.FacebookUserName));
                    Location pi = new Location("");
                    pi.setLatitude(Double.parseDouble(obj.optString("Latitude")));
                    pi.setLongitude(Double.parseDouble(obj.optString("Longitude")));

                    long distance = (long) pi.distanceTo(current);

                    map.put(MyConstant.InstagramDistance, "" + distance);

                    map.put(MyConstant.TYPE, MyConstant.FACEBOOK_USER);

                    jsonList.add(map);
                }

                if (obj.optString(MyConstant.TwitterUserName).length() > 1) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(MyConstant.TwitterUserName, obj.optString(MyConstant.TwitterUserName));

                    Location pi = new Location("");
                    pi.setLatitude(Double.parseDouble(obj.optString("Latitude")));
                    pi.setLongitude(Double.parseDouble(obj.optString("Longitude")));
                    long distance = (long) pi.distanceTo(current);
                    map.put(MyConstant.InstagramDistance, "" + distance);
                    map.put(MyConstant.TYPE, MyConstant.TWITTER_USER);
                    jsonList.add(map);

                }
            }

            Log.e("PUT", "jsonList2: " + jsonArray.length());
            /*for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);

                //FAcebook User
                if (!obj.optString(MyConstant.FacebookUserName).isEmpty() && !obj.optString(MyConstant.FacebookId).equals("null")) {

                    HashMap<String, String> map = new HashMap<>();


                    map.put(MyConstant.FacebookId, obj.optString(MyConstant.FacebookId));

                    map.put(MyConstant.FacebookUserName, obj.optString(MyConstant.FacebookUserName));
                    Location pi = new Location("");
                    pi.setLatitude(Double.parseDouble(obj.optString("Latitude")));
                    pi.setLongitude(Double.parseDouble(obj.optString("Longitude")));

                    long distance = (long) pi.distanceTo(current);

                    map.put(MyConstant.InstagramDistance, "" + distance);

                    map.put(MyConstant.TYPE, MyConstant.FACEBOOK_USER);

                    jsonList.add(map);

                }
            }
            Log.e("PUT", "jsonList3: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);

                //Twitter User

                if (obj.optString(MyConstant.TwitterUserName).length()>1) {

                    Log.e("USERNAMETWITTER","USERNAME"+obj.optString(MyConstant.TwitterUserName));

                    HashMap<String, String> map = new HashMap<>();
                    map.put(MyConstant.TwitterUserName, obj.optString(MyConstant.TwitterUserName));

                    Location pi = new Location("");
                    pi.setLatitude(Double.parseDouble(obj.optString("Latitude")));
                    pi.setLongitude(Double.parseDouble(obj.optString("Longitude")));
                    long distance = (long) pi.distanceTo(current);
                    map.put(MyConstant.InstagramDistance, "" + distance);
                    map.put(MyConstant.TYPE, MyConstant.TWITTER_USER);
                    jsonList.add(map);

                    Log.e("PUT", "Distance: " + distance);
                }
            }

            Log.e("PUT", "jsonList: " + jsonArray.length());*/
            Collections.sort(jsonList, new MapComparator(MyConstant.InstagramDistance));


            rotateOrNot();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    class MapComparator implements Comparator<HashMap<String, String>> {
        private final String key;

        public MapComparator(String key) {
            this.key = key;

        }

        public int compare(HashMap<String, String> first,
                           HashMap<String, String> second) {
            // TODO: Null checking, both for maps and values


            Integer firstValue = Integer.valueOf(first.get(key));
            Integer secondValue = Integer.valueOf(second.get(key));
            return firstValue.compareTo(secondValue);
        }
    }


    //*****************************************************  service  *************************************************************************


    public void getInstagramData(final int position) {


        String url = MyConstant.getInstagramUserUrl(jsonList.get(position).get((MyConstant.JsonInstagramUserId)));//("3021402478");
        Log.e("Index : " + position, url);


        MyUtil.execute(new Super_AsyncTask(context, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                Log.e("visit", "output" + jsonList.toString());
                Log.e("full", "output" + output.toString());

                try {
                    JSONObject object = new JSONObject(output);
                    if (object.getJSONObject("meta").getString("code").equals("200")) {

                        JSONObject obj2 = object.getJSONObject("data");
                        Log.e("full", "data" + obj2);

                        HashMap<String, String> map = new HashMap<>();

                        map.put(MyConstant.InstagramUsername, obj2.getString(MyConstant.InstagramUsername));
                        map.put(MyConstant.InstagramPicUrl, obj2.getString(MyConstant.InstagramPicUrl));
                        map.put(MyConstant.InstagramFullName, obj2.getString(MyConstant.InstagramFullName));
                        map.put(MyConstant.InstagramId, obj2.getString(MyConstant.InstagramId));
                        map.put(MyConstant.InstagramDistance, jsonList.get(position).get(MyConstant.InstagramDistance));

                        map.put(MyConstant.TYPE, MyConstant.INSTAGRAM_USER);
                        mainList.add(map);
                        Log.e("instagram", "instgramdistance" + jsonList.get(position).get(MyConstant.InstagramDistance));

                        Log.e("instagram", "mainList" + mainList);
                        setData();

                    } else {

                    }
                } catch (Exception ex) {
                    Log.e("Exception is", ex.toString());
                }
            }
        }, false));
    }


    public void getFacebookData(final int position) {

        Log.e("MyConstant.FacebookId", "" + MyConstant.FacebookId);
        Log.e("jsonList.get", "" + jsonList.get(position));
        final String url = MyConstant.getFacebookProfile(jsonList.get(position).get(MyConstant.FacebookId));

        final HashMap<String, String> map = new HashMap<>();
        MyUtil.execute(new Super_AsyncTask(context, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                map.put(MyConstant.FacewbookPicUrl, url);
                map.put(MyConstant.TYPE, MyConstant.FACEBOOK_USER);
                map.put(MyConstant.FacebookId, jsonList.get(position).get(MyConstant.FacebookId));
                map.put(MyConstant.FacebookUserName, jsonList.get(position).get(MyConstant.FacebookUserName));
                map.put(MyConstant.InstagramDistance, jsonList.get(position).get(MyConstant.InstagramDistance));
                mainList.add(map);
                setData();

            }
        }, false));

    }


    public void getTwitterData(final int position) {


        final String url = MyConstant.getTwitterProfile(jsonList.get(position).get(MyConstant.TwitterUserName));
        Log.e("Index : " + position, url);
        Log.e("TwitterUserName", "" + jsonList.get(position).get(MyConstant.TwitterUserName));
        final HashMap<String, String> map = new HashMap<>();
        MyUtil.execute(new Super_AsyncTask(context, url, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String output) {

                map.put(MyConstant.TwitterPicUrl, url);
                map.put(MyConstant.TYPE, MyConstant.TWITTER_USER);
                map.put(MyConstant.TwitterUserName, jsonList.get(position).get(MyConstant.TwitterUserName));
                map.put(MyConstant.InstagramDistance, jsonList.get(position).get(MyConstant.InstagramDistance));

                mainList.add(map);
                setData();

            }
        }, false));

    }


}
